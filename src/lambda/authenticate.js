require('dotenv').load()

const { MAKERLOG_API_ROOT, MAKERLOG_CLIENT_ID } = process.env;

export async function handler(event, context) {
    try {
        return {
            statusCode: 301,
            body: '',
            headers: {
                'Location': `${MAKERLOG_API_ROOT}/oauth/authorize/?client_id=${MAKERLOG_CLIENT_ID}&scope=user:read%20tasks:read&response_type=token`,
            }
        }
    } catch (e) {
        console.log(e)
    }
}