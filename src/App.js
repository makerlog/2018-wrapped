import React from 'react';
import queryString from 'query-string';
import {getStats, getUser, getWrappedStats, me} from './makerlog';
import axios from './axios';
import Spinner from "./components/Spinner";
import abbreviate from 'number-abbreviate';
import Emoji from "./components/Emoji";
import {faDotCircle} from "@fortawesome/free-solid-svg-icons/faDotCircle";
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons/faCheckCircle";
import {faTwitter} from "@fortawesome/free-brands-svg-icons/faTwitter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CountUp from 'react-countup';
import FullName, {fullName} from "./components/FullName";
import {Helmet} from "react-helmet";

// idea: maker score this year

function url(path) {
    return `${process.env.REACT_APP_UI_ROOT}${path}`
}

function api_url(path) {
    return `${process.env.REACT_APP_API_ROOT}${path}`
}

function processRoute(url) {
    var pattern = /\B@[a-z0-9_-]+/gi;
    return url.match(pattern);
}

const renderTaskIcon = (task) => {
    let doneIcon = faCheckCircle;
    let remainingIcon = faDotCircle;
    let doneColor = "#27ae60"
    let remainingColor = "#f39c12";

    return (task.done) ?
        <FontAwesomeIcon icon={doneIcon} color={doneColor} />
        : <FontAwesomeIcon icon={remainingIcon} color={remainingColor} />
}


class App extends React.Component {
    state = {
        loggedIn: false,
        public: false,
        user: null,
        token: null,
        loading: true,
        errored: false,
        stats: null,
        wrappedStats: null,
        // refreshToken: null,
    }

    async componentDidMount() {
        if (processRoute(window.location.href) && processRoute(window.location.href).length === 1) {
            const username = processRoute(window.location.href)[0].replace('@','');
            try {
                const user = await getUser(username);
                this.setState({ public: true, user: user })
                this.getUserStats(user.id)
            } catch (e) {
                this.setState({
                    public: false,
                    token: null,
                    user: null,
                })
            }

            return;
        }

        if (window.location.hash) {
            // we have a fragment.
            const parsed = queryString.parse(window.location.hash.substr(1))
            if (parsed.access_token) {
                await this.setState({
                    token: parsed.access_token,
                })
                localStorage.setItem('token', parsed.access_token)
                window.history.pushState({}, "My 2018 Wrapped | Makerlog", "/");
            }
        } else if (window.location.search) {
            // refresh token
        } else if (localStorage.getItem('token')) {
            await this.setState({
                token: localStorage.getItem('token')
            })
        }

        if (this.state.token) {
            axios.defaults.headers.common['Authorization'] = `Bearer ${this.state.token}`;

            try {
                const user = await me();
                this.setState({ loggedIn: true, user: user })
                this.getUserStats(user.id)
            } catch (e) {
                this.setState({
                    loggedIn: false,
                    token: null,
                    user: null,
                })
            }
        }
    }

    getUserStats = async (uid=null) => {
        try {
            const stats = await getStats(uid);
            const wrappedStats = await getWrappedStats(uid);
            this.setState({
                stats,
                wrappedStats,
                loading: false,
                errored: false,
            })
        } catch (e) {
            this.setState({
                loading: false,
                errored: true,
            })
        }
    }

    getMakerScore = () => {
        // streak + tda + praise_received
        // factor in date joined
    }

    logout = () => {
        this.setState({
            loggedIn: false,
            token: null,
            user: null,
        })
        localStorage.removeItem('token');
        window.location.replace(process.env.REACT_APP_ROOT)
    }

    renderLoggedOut = () => (
        <section id="main-hero" className="hero is-success is-fullheight">
            <Helmet>
                <title>Your 2018 Wrapped | Makerlog</title>
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:site" content="@getmakerlog" />
                <meta property="og:url" content={process.env.REACT_APP_ROOT} />
                <meta property="og:title" content="Makerlog's 2018 Wrapped" />
                <meta property="og:description" content="2018 was an amazing year. See how productive you were with 2018 Wrapped. #MyYearInTasks" />
            </Helmet>
            <div className={"overlay"}></div>
            <div className="hero-body">
                <div className="container">
                    <h1 className="logo title">
                        Makerlog
                    </h1>
                    <h1 className="main-title title is-1">
                        2018 Wrapped
                    </h1>
                    <h2 className="subtitle">
                        This was the year of the maker, and the community grew like never before seen. <br />
                        See how you shipped this 2018.
                    </h2>
                    <a className="button is-primary is-rounded" href={"/.netlify/functions/authenticate/"}>
                        Get started
                    </a>
                    <a className={"button is-text is-white is-inverted is-small"} style={{background: 'transparent', textDecoration: 'none', marginTop: 5, marginLeft: 8}} href={"https://getmakerlog.com/"}>
                        Don't have a Makerlog account?
                    </a>
                </div>
            </div>
        </section>
    )

    renderTweetButton = () => {
        const url = `${process.env.REACT_APP_ROOT}/@${this.state.user.username}`;
        const text = `Here's my 2018 Wrapped from @getmakerlog! \n #MyYearInTasks`;

        return (
            <a href={`https://twitter.com/share?text=${encodeURIComponent(text)}&url=${url}`}
               className="button is-info is-large is-rounded" style={{backgroundColor: "#1b95e0"}} target="_blank">
                <span className={"icon"} style={{marginRight: 5}}><FontAwesomeIcon icon={faTwitter} /></span> <strong>Tweet your achievements</strong>
            </a>
        )
    }

    renderLoggedIn = () => (
        <div>
            <div className={"container"} style={{paddingTop: 30}}>
                <div className={"columns"}>
                    <div className={"column is-8 is-offset-2"}>
                        <div className={"media"} style={{marginBottom: 20}}>
                            <div className={"media-left"}>
                                <figure className={"image is-64x64"}>
                                    <img className={"is-rounded"} src={this.state.user.avatar} alt={"The user"} />
                                </figure>
                            </div>
                            <div className={"media-content"}>
                                <h1 className={"title is-4 has-text-white"}>
                                    Hi, {this.state.user.first_name ? this.state.user.first_name : this.state.user.username}!
                                </h1>
                                <h1 className={"subtitle is-4 has-text-white has-text-weight-light"}>
                                    Here's your year in review.
                                </h1>
                            </div>
                            <div className={"media-right"}>
                                <button className={"button is-outlined is-inverted is-primary is-rounded is-small"} onClick={this.logout}>Log out</button>
                            </div>
                        </div>
                        <div className={"card"}>
                            <div className={"card-content"}>
                                {this.state.loading && <center>
                                    <Spinner text={"Looking back at your year..."} color="#20bb7a" />
                                </center>}
                                {!this.state.loading && !this.state.errored &&
                                <div>
                                    <h1 className={"title is-4"}>Your stats so far️</h1>
                                    <p className={"subtitle is-6"}>
                                        Here's all your stats on Makerlog so far... plus the all-new <strong>Maker Score</strong>™ which uses a super-secret algorithm to calculate how well you've done!
                                    </p>
                                    <div style={{padding: 30}}>
                                        <nav className="level">
                                            <div className="level-item has-text-centered">
                                                <div>
                                                    <p className="heading"><Emoji emoji={"🏆️"}/> Maker Score™</p>
                                                    <p className="title is-2">{abbreviate(this.state.user.maker_score, 1)}</p>
                                                </div>
                                            </div>
                                            <div className="level-item has-text-centered">
                                                <div>
                                                    <p className="heading"><Emoji emoji={"🔥️"}/> Streak</p>
                                                    <p className="title is-2">{this.state.user.streak}</p>
                                                </div>
                                            </div>
                                            <div className="level-item has-text-centered">
                                                <div>
                                                    <p className="heading"><Emoji emoji={"👏️"}/> Praise received</p>
                                                    <p className="title is-2">{abbreviate(this.state.stats.praise_received, 1)}</p>
                                                </div>
                                            </div>
                                            <div className="level-item has-text-centered">
                                                <div>
                                                    <p className="heading"><Emoji emoji={"🏁️"}/> Tasks/day</p>
                                                    <p className="title is-2">{abbreviate(this.state.stats.tda, 1)}</p>
                                                </div>
                                            </div>
                                        </nav>
                                    </div>

                                    {this.state.wrappedStats.top_praised.length !== 0 &&
                                    <>
                                        <hr />

                                        <h1 className={"title is-4"}>Your most popular tasks️</h1>
                                        <p className={"subtitle is-6"}>
                                            It's been an intense year with many great tasks posted... Here are your most popular ones!
                                        </p>
                                        <div style={{padding: 30, paddingTop: 20}}>
                                            {this.state.wrappedStats.top_praised &&
                                            this.state.wrappedStats.top_praised.map(
                                                t => <a href={url(`/tasks/${t.id}/`)}>
                                                    <div className={"Entry"}>
                                                        {renderTaskIcon(t)} {t.content} {t.comment_count > 0 ? <span style={{display: "inline-block"}} className={"PraiseIndicator has-text-grey-light"}><Emoji emoji={"💬"}/>{t.comment_count}</span> : null}
                                                    </div>
                                                </a>
                                            )
                                            }
                                        </div>
                                    </>
                                    }

                                    {this.state.wrappedStats.products_this_year.length !== 0 &&
                                    <>
                                        <hr />

                                        <h1 className={"title is-4"}>Products this year</h1>
                                        <p className={"subtitle is-6"}>
                                            Here's all the products you've launched and worked on this year.
                                        </p>
                                        <div style={{padding: 30, paddingTop: 20}}>
                                            {this.state.wrappedStats.products_this_year &&
                                            this.state.wrappedStats.products_this_year.map(
                                                p => <div className={"media"}>
                                                    <div className={"media-left"}>
                                                        <figure className={"icon image is-64x64"}>
                                                            <img src={p.icon || 'http://via.placeholder.com/500?text=No+icon'} />
                                                        </figure>
                                                    </div>
                                                    <div className={"media-content"}>
                                                        <p>
                                                            <strong>{p.name}</strong>
                                                            <br />
                                                            {p.description}
                                                        </p>
                                                    </div>
                                                </div>
                                            )
                                            }
                                        </div>

                                        <hr />

                                        <h1 className={"title is-4"}>Most shipped product</h1>
                                        <p className={"subtitle is-6"}>
                                            And your most shipped product is...
                                        </p>
                                        <div style={{padding: 30, paddingTop: 20}}>
                                            {this.state.wrappedStats.most_shipped_product &&
                                            <div>
                                                <div className={"media"}>
                                                    <div className={"media-left"}>
                                                        <figure className={"icon image is-128x128"}>
                                                            <img src={this.state.wrappedStats.most_shipped_product.product.icon || 'http://via.placeholder.com/500?text=No+icon'} />
                                                        </figure>
                                                    </div>
                                                    <div className={"media-content"}>
                                                        <p>
                                                            <strong className={"title is-3"}>{this.state.wrappedStats.most_shipped_product.product.name}</strong>
                                                            <br />
                                                            <div className={"subtitle is-5"}>
                                                                {this.state.wrappedStats.most_shipped_product.product.description}
                                                            </div>
                                                            <small className={"has-text-grey"}>You shipped {this.state.wrappedStats.most_shipped_product.task_count} tasks for this product!</small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            }
                                        </div>
                                    </>
                                    }


                                    <div className="pyro">
                                        <div className="before"></div>
                                        <div className="after"></div>
                                    </div>

                                    <hr />

                                    <h1 className={"title is-4"}>The final count</h1>
                                    <p className={"subtitle is-6"}>
                                        And your number of tasks shipped this year is...
                                    </p>
                                    <div style={{padding: 30, paddingTop: 20}}>
                                        <center>
                                            <nav className="level">
                                                <div className="level-item has-text-centered">
                                                    <div>
                                                        <p className="heading"><Emoji emoji={"✅"}/> Tasks done in 2018</p>
                                                        {this.state.wrappedStats.done_this_year === 0 &&
                                                        <p className="title" style={{fontSize: '5rem'}}>You did nothing</p>
                                                        }

                                                        {this.state.wrappedStats.done_this_year > 0 &&
                                                        <p className="title" style={{fontSize: '10rem'}}>
                                                            <CountUp end={this.state.wrappedStats.done_this_year} />
                                                        </p>
                                                        }
                                                    </div>
                                                </div>
                                            </nav>
                                        </center>
                                    </div>
                                </div>
                                }
                            </div>
                            {!this.state.loading &&
                            <div className={"card-footer"}>
                                <p className={"title is-4"}>
                                    <center>Share your achievements this year! <a href={"https://twitter.com/search?f=tweets&q=%23MyYearInTasks&src=typd"}>#MyYearInTasks</a></center>
                                </p>
                                <center>
                                    {this.renderTweetButton()}
                                </center>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

    renderPublicPage = () => (
        <div>
            <Helmet>
                <title>{fullName(this.state.user)}'s 2018 Wrapped</title>
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:site" content="@getmakerlog" />
                <meta property="og:url" content={process.env.REACT_APP_ROOT} />
                <meta property="og:title" content={`${fullName(this.state.user)}'s 2018 Wrapped`} />
                <meta property="og:description" content={`2018 was an amazing year. See how productive ${fullName(this.state.user)} was. #MyYearInTasks`} />
                <meta name="twitter:image" content={api_url(`/users/${this.state.user.id}/wrapped_image/`)} />
            </Helmet>
            <div className={"container"} style={{paddingTop: 30}}>
                <div className={"columns"}>
                    <div className={"column is-8 is-offset-2"}>
                        <div className={"media"} style={{marginBottom: 20}}>
                            <div className={"media-left"}>
                                <figure className={"image is-64x64"}>
                                    <img className={"is-rounded"} src={this.state.user.avatar} alt={"The user"} />
                                </figure>
                            </div>
                            <div className={"media-content"}>
                                <h1 className={"title is-4 has-text-white"}>
                                    <FullName user={this.state.user} />'s 2018
                                </h1>
                                <h1 className={"subtitle is-4 has-text-white has-text-weight-light"}>
                                    Here's their year in review.
                                </h1>
                            </div>
                        </div>
                        <div className={"card"}>
                            <div className={"card-content"}>
                                {this.state.loading && <center>
                                    <Spinner text={"Looking back at your year..."} color="#20bb7a" />
                                </center>}
                                {!this.state.loading && !this.state.errored &&
                                <div>
                                    <h1 className={"title is-4"}>
                                        Stats so far
                                    ️</h1>
                                    <p className={"subtitle is-6"}>
                                        <FullName user={this.state.user} /> has been having an amazing year on Makerlog! Below are their stats.
                                    </p>
                                    <div style={{padding: 30}}>
                                        <nav className="level">
                                            <div className="level-item has-text-centered">
                                                <div>
                                                    <p className="heading"><Emoji emoji={"🏆️"}/> Maker Score™</p>
                                                    <p className="title is-2">{abbreviate(this.state.user.maker_score, 1)}</p>
                                                </div>
                                            </div>
                                            <div className="level-item has-text-centered">
                                                <div>
                                                    <p className="heading"><Emoji emoji={"🔥️"}/> Streak</p>
                                                    <p className="title is-2">{this.state.user.streak}</p>
                                                </div>
                                            </div>
                                            <div className="level-item has-text-centered">
                                                <div>
                                                    <p className="heading"><Emoji emoji={"👏️"}/> Praise received</p>
                                                    <p className="title is-2">{abbreviate(this.state.stats.praise_received, 1)}</p>
                                                </div>
                                            </div>
                                            <div className="level-item has-text-centered">
                                                <div>
                                                    <p className="heading"><Emoji emoji={"🏁️"}/> Tasks/day</p>
                                                    <p className="title is-2">{abbreviate(this.state.stats.tda, 1)}</p>
                                                </div>
                                            </div>
                                        </nav>
                                    </div>

                                    {this.state.wrappedStats.top_praised.length !== 0 &&
                                        <>
                                            <hr />

                                            <h1 className={"title is-4"}>Most popular tasks️</h1>
                                            <p className={"subtitle is-6"}>
                                                It's been an intense year with many great tasks posted... Here are <FullName user={this.state.user} />'s most popular ones!
                                            </p>
                                            <div style={{padding: 30, paddingTop: 20}}>
                                                {this.state.wrappedStats.top_praised &&
                                                this.state.wrappedStats.top_praised.map(
                                                    t => <a target="_blank" href={url(`/tasks/${t.id}/`)}>
                                                        <div className={"Entry"}>
                                                            {renderTaskIcon(t)} {t.content} {t.praise > 0 ? <span style={{display: "inline-block"}} className={"PraiseIndicator has-text-grey-light"}><Emoji emoji={"👏"}/>{t.praise}</span> : null} {t.comment_count > 0 ? <span style={{display: "inline-block"}} className={"PraiseIndicator has-text-grey-light"}><Emoji emoji={"💬"}/>{t.comment_count}</span> : null}
                                                        </div>
                                                    </a>
                                                )
                                                }
                                            </div>
                                        </>
                                    }

                                    {this.state.wrappedStats.products_this_year.length !== 0 &&
                                        <>
                                            <hr />

                                            <h1 className={"title is-4"}>Products this year</h1>
                                            <p className={"subtitle is-6"}>
                                                Here's all the products they've launched and worked on this year.
                                            </p>
                                            <div style={{padding: 30, paddingTop: 20}}>
                                                {this.state.wrappedStats.products_this_year &&
                                                this.state.wrappedStats.products_this_year.map(
                                                    p => <div className={"media"}>
                                                        <div className={"media-left"}>
                                                            <figure className={"icon image is-64x64"}>
                                                                <img src={p.icon || 'http://via.placeholder.com/500?text=No+icon'} />
                                                            </figure>
                                                        </div>
                                                        <div className={"media-content"}>
                                                            <p>
                                                                <strong>{p.name}</strong>
                                                                <br />
                                                                {p.description}
                                                            </p>
                                                        </div>
                                                    </div>
                                                )
                                                }
                                            </div>

                                            <hr />

                                            <h1 className={"title is-4"}>Most shipped product</h1>
                                            <p className={"subtitle is-6"}>
                                                And <FullName user={this.state.user} />'s most shipped product is...
                                            </p>
                                            <div style={{padding: 30, paddingTop: 20}}>
                                                {this.state.wrappedStats.most_shipped_product &&
                                                <div>
                                                    <div className={"media"}>
                                                        <div className={"media-left"}>
                                                            <figure className={"icon image is-128x128"}>
                                                                <img src={this.state.wrappedStats.most_shipped_product.product.icon || 'http://via.placeholder.com/500?text=No+icon'} />
                                                            </figure>
                                                        </div>
                                                        <div className={"media-content"}>
                                                            <p>
                                                                <strong className={"title is-3"}>{this.state.wrappedStats.most_shipped_product.product.name}</strong>
                                                                <br />
                                                                <div className={"subtitle is-5"}>
                                                                    {this.state.wrappedStats.most_shipped_product.product.description}
                                                                </div>
                                                                <small className={"has-text-grey"}>You shipped {this.state.wrappedStats.most_shipped_product.task_count} tasks for this product!</small>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                }
                                            </div>
                                        </>
                                    }


                                    <div className="pyro">
                                        <div className="before"></div>
                                        <div className="after"></div>
                                    </div>

                                    <hr />

                                    <h1 className={"title is-4"}>The final count</h1>
                                    <p className={"subtitle is-6"}>
                                        And <FullName user={this.state.user} />'s number of tasks shipped this year is...
                                    </p>
                                    <div style={{padding: 30, paddingTop: 20}}>
                                        <center>
                                            <nav className="level">
                                                <div className="level-item has-text-centered">
                                                    <div>
                                                        <p className="heading"><Emoji emoji={"✅"}/> Tasks done in 2018</p>
                                                        {this.state.wrappedStats.done_this_year === 0 &&
                                                            <p className="title" style={{fontSize: '5rem'}}><FullName user={this.state.user} /> did nothing</p>
                                                        }
                                                            
                                                        {this.state.wrappedStats.done_this_year > 0 &&
                                                            <p className="title" style={{fontSize: '10rem'}}>
                                                                <CountUp end={this.state.wrappedStats.done_this_year} />
                                                            </p>
                                                        }
                                                    </div>
                                                </div>
                                            </nav>
                                        </center>
                                    </div>
                                </div>
                                }
                            </div>
                            {!this.state.loading &&
                                <div className={"card-footer"}>
                                    <p className={"title is-4"}>
                                        <center>Get your own 2018 Wrapped! <a href={"https://twitter.com/search?f=tweets&q=%23MyYearInTasks&src=typd"}>#MyYearInTasks</a></center>
                                    </p>
                                    <center>
                                        <a href={`/.netlify/functions/authenticate/`}
                                           className="button is-primary is-large is-rounded" target="_blank">
                                            Get your own page
                                        </a>
                                    </center>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

    render() {
        if (!this.state.loggedIn && !this.state.public) {
            return this.renderLoggedOut()
        } else if (this.state.public) {
            return this.renderPublicPage();
        } else {
            return this.renderLoggedIn();
        }
    }
}

// top praised tasks
// top worked on products
// time spent on each product

export default App;