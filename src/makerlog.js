import axios from './axios';

export async function me() {
    const endpoint = '/me/';
    const { data } = await axios.get(endpoint);
    return data
}

export async function getUser(username) {
    const endpoint = `/users/username/${username}/`;
    const { data } = await axios.get(endpoint);
    return data
}


export async function getStats(id) {
    const response = await axios.get(id ? `/users/${id}/stats/` : '/me/stats/');
    return response.data;
}

export async function getWrappedStats(id) {
    const response = await axios.get(`/users/${id}/wrapped/`);
    return response.data;
}
