import React from 'react';
import PropTypes from 'prop-types';

export function fullName(user, prependUsername=false) {
    return user.first_name ? `${user.first_name} ${user.last_name}` : (prependUsername ? `@${user.username}` : user.username)
}

const FullName = ({ user, prependUsername=false }) => (
    <span>{fullName(user, prependUsername)}</span>
)

FullName.propTypes = {
    user: PropTypes.object.isRequired,
    prependUsername: PropTypes.bool,
}

export default FullName;