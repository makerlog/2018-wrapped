import React from 'react';
import emoji from 'react-easy-emoji';
import emojiSupport from 'detect-emoji-support';

const Emoji = (props) => (
    <span className="emoji" role="img">{emojiSupport() ? props.emoji : null}</span>
)

export default Emoji;
